package net.kernevez.hibernate.entity.withpk;

public interface PkCurrencyCustomRepository {
    PkCurrencyEntity getCurrencyByNaturalIsoCode(String isoCode);
}
