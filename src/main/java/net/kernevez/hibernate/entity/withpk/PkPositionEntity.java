package net.kernevez.hibernate.entity.withpk;

import net.kernevez.hibernate.entity.withnatural.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "PK_POSITION")
public class PkPositionEntity extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PK_POSITION")
    @SequenceGenerator(name = "SEQ_PK_POSITION", sequenceName = "SEQ_PK_POSITION", allocationSize = 1)
    private Long id;

    @Embedded
    private PkAmount netAmount;
    @Embedded
    private PkAmount grossAmount;

    public PkAmount getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(PkAmount netAmount) {
        this.netAmount = netAmount;
    }

    public PkAmount getGrossAmount() {
        return grossAmount;
    }

    public Long getId() {
        return id;
    }

}
