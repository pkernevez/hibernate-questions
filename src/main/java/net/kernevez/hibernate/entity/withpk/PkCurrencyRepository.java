package net.kernevez.hibernate.entity.withpk;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PkCurrencyRepository extends JpaRepository<PkCurrencyEntity, Long>, PkCurrencyCustomRepository {
}
