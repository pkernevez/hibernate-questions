package net.kernevez.hibernate.entity.withpk;

import org.hibernate.Session;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class PkCurrencyCustomRepositoryImpl implements PkCurrencyCustomRepository {
    @PersistenceContext
    private EntityManager em;

    @Override
    public PkCurrencyEntity getCurrencyByNaturalIsoCode(String isoCode) {
        return em.unwrap(Session.class)
                 .bySimpleNaturalId(PkCurrencyEntity.class)
                 .load(isoCode);
    }
}
