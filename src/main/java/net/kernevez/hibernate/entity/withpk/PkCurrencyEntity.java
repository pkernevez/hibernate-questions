package net.kernevez.hibernate.entity.withpk;

import net.kernevez.hibernate.entity.withnatural.BaseEntity;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "PK_CURRENCY")
// Serializable because of : https://hibernate.atlassian.net/browse/HHH-7668
public class PkCurrencyEntity extends BaseEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PK_CURRENCY")
    @SequenceGenerator(name = "SEQ_PK_CURRENCY", sequenceName = "SEQ_PK_CURRENCY", allocationSize = 1)
    private Long id;

    @NaturalId
    @NotNull
    private String isoCode;

    public Long getId() {
        return id;
    }

    public String getIsoCode() {
        return isoCode;
    }
}
