package net.kernevez.hibernate.entity.withpk;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PkPositionRepository extends JpaRepository<PkPositionEntity, Long> {

    List<PkPositionEntity> findAllByIdIn(List<Long> ids);
}
