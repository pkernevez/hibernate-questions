package net.kernevez.hibernate.entity.withnatural;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrencyRepository extends JpaRepository<CurrencyEntity, Long>, CurrencyCustomRepository {
}
