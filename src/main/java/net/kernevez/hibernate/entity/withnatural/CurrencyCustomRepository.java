package net.kernevez.hibernate.entity.withnatural;

public interface CurrencyCustomRepository {
    CurrencyEntity getCurrencyByNaturalIsoCode(String isoCode);
}
