package net.kernevez.hibernate.entity.withnatural;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.util.Objects;

@Embeddable
public class Amount {

    protected BigDecimal quantity;
    @JoinColumn(referencedColumnName = "isoCode", nullable = false)
//    @ManyToOne(fetch = FetchType.LAZY)
    @ManyToOne()
    protected CurrencyEntity currency;

    public BigDecimal getQuantity() {
        return quantity;
    }

    public CurrencyEntity getCurrency() {
        return currency;
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Amount other)) return false;
        if (getQuantity() == null && other.getQuantity() == null) {
            return Objects.equals(getCurrency(), other.getCurrency());
        }
        if (getQuantity() == null || other.getQuantity() == null) {
            return false;
        }
        return getQuantity().compareTo(other.getQuantity()) == 0 &&
               Objects.equals(getCurrency(), other.getCurrency());
    }

    @Override
    public final int hashCode() {
        if (getQuantity() == null) {
            return Objects.hash(null, getCurrency());
        }
        return Objects.hash(getQuantity().stripTrailingZeros(), getCurrency());
    }

    @Override
    public String toString() {
        return "" + quantity + " " + currency.getIsoCode();
    }

}
