package net.kernevez.hibernate.entity.withnatural;

import org.hibernate.Session;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class CurrencyCustomRepositoryImpl implements CurrencyCustomRepository {
    @PersistenceContext
    private EntityManager em;

    @Override
    public CurrencyEntity getCurrencyByNaturalIsoCode(String isoCode) {
        return em.unwrap(Session.class)
                 .bySimpleNaturalId(CurrencyEntity.class)
                 .load(isoCode);
    }
}
