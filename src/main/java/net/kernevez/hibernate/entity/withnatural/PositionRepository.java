package net.kernevez.hibernate.entity.withnatural;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PositionRepository extends JpaRepository<PositionEntity, Long> {

    List<PositionEntity> findAllByIdIn(List<Long> ids);
}
