package net.kernevez.hibernate.entity.withnatural;

import javax.persistence.*;

@Entity
@Table(name = "POSITION")
public class PositionEntity extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_POSITION")
    @SequenceGenerator(name = "SEQ_POSITION", sequenceName = "SEQ_POSITION", allocationSize = 1)
    private Long id;

    @Embedded
    private Amount netAmount;
    @Embedded
    private Amount grossAmount;

    public Amount getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(Amount netAmount) {
        this.netAmount = netAmount;
    }

    public Amount getGrossAmount() {
        return grossAmount;
    }

    public Long getId() {
        return id;
    }
}
