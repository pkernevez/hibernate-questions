package net.kernevez.hibernate.entity.withpk;

import net.kernevez.hibernate.conf.JpaAuditingConfiguration;
import net.kernevez.hibernate.conf.TimeZoneConfig;
import org.hibernate.SessionFactory;
import org.hibernate.stat.Statistics;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.junit.jupiter.Testcontainers;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@Import({JpaAuditingConfiguration.class, TimeZoneConfig.class})
@Transactional
@Testcontainers
class PkPositionEntityTest {

    @Autowired
    private EntityManager em;

    @Autowired
    private PkPositionRepository sut;

    @Autowired
    private EntityManagerFactory entityManagerFactory;


    @Nested
    class WithEager {
        // Those tests need to have a Eager strategy in class PkAmount
        //    @ManyToOne()
        //    protected PkCurrencyEntity currency;
        @Test
        void testPassWithAmountEagerStrategy() {
            // Given
            SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
            Statistics stats = sessionFactory.getStatistics();
            stats.clear();
            em.clear();

            // When first load, need to execute a query using the natural id
            PkPositionEntity pos1chf = sut.getReferenceById(1L);
            assertEquals(1, pos1chf.getId());
            assertEquals(new BigDecimal("123456.7890"), pos1chf.getNetAmount().getQuantity());
            assertEquals("CHF", pos1chf.getNetAmount().getCurrency().getIsoCode());
            assertEquals(2, stats.getEntityLoadCount());
            assertEquals(0, stats.getQueryExecutionCount());
            // First request: eager, select all the fields of position and 2 times the net and gross currencies (this row has the same currency but it's a particular case)
            // select positionen0_.*, currencyen1_.id as id1_0_1_, currencyen1_.*, currencyen2_.* from test.position positionen0_ inner join test.currency currencyen1_ on positionen0_.gross_amount_currency_iso_code=currencyen1_.iso_code inner join test.currency currencyen2_ on positionen0_.net_amount_currency_iso_code=currencyen2_.iso_code where positionen0_.id=?
            //
            // With the PK, we don't have anymore a 2nd request, all the fields are used to create the Currency
            assertEquals(1, stats.getPrepareStatementCount());

            // Now reload
            PkPositionEntity pos2chf = sut.getReferenceById(2L);
            assertEquals(2, pos2chf.getId());
            assertEquals(new BigDecimal("223456.7890"), pos2chf.getNetAmount().getQuantity());
            assertEquals("CHF", pos2chf.getNetAmount().getCurrency().getIsoCode());
            assertEquals(3, stats.getEntityLoadCount());
            assertEquals(0, stats.getQueryExecutionCount());
            // One additional request: eager, select all the fields of position and 2 times the net and gross currencies (this row has the same currency but it's a particular case)
            // select positionen0_.*, currencyen1_.id as id1_0_1_, currencyen1_.*, currencyen2_.* from test.position positionen0_ inner join test.currency currencyen1_ on positionen0_.gross_amount_currency_iso_code=currencyen1_.iso_code inner join test.currency currencyen2_ on positionen0_.net_amount_currency_iso_code=currencyen2_.iso_code where positionen0_.id=?
            assertEquals(2, stats.getPrepareStatementCount());
        }

    }

    @Nested
    class WithLazy {
        // Those tests need to have a Eager strategy in class PkAmount
        //    @ManyToOne(fetch = FetchType.LAZY)
        //    protected PkCurrencyEntity currency;

        @Test
        void testPassWithAmountLazyStrategy() {

            // Given
            SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
            Statistics stats = sessionFactory.getStatistics();
            stats.clear();
            em.clear();

            // When first load, need to execute a query using the natural id
            PkPositionEntity pos1chf = sut.getReferenceById(1L);
            assertEquals(1, pos1chf.getId());
            assertEquals(new BigDecimal("123456.7890"), pos1chf.getNetAmount().getQuantity());
            assertEquals("CHF", pos1chf.getNetAmount().getCurrency().getIsoCode());
            assertEquals(2, stats.getEntityLoadCount());
            assertEquals(0, stats.getQueryExecutionCount());
            // First request: lazy, select all the fields of position
            // select positionen0_.*, currencyen1_.id as id1_0_1_ from test.position positionen0_ inner join test.currency currencyen1_ on positionen0_.gross_amount_currency_iso_code=currencyen1_.iso_code inner join test.currency currencyen2_ on positionen0_.net_amount_currency_iso_code=currencyen2_.iso_code where positionen0_.id=?
            // And one for the currency (instead of 2 with the naturalId)
            // select currencyen0_.* from test.currency currencyen0_ where currencyen0_.iso_code=?
            assertEquals(2, stats.getPrepareStatementCount());

            // Now reload
            PkPositionEntity pos2chf = sut.getReferenceById(2L);
            assertEquals(2, pos2chf.getId());
            assertEquals(new BigDecimal("223456.7890"), pos2chf.getNetAmount().getQuantity());
            assertEquals("CHF", pos2chf.getNetAmount().getCurrency().getIsoCode());
            assertEquals(3, stats.getEntityLoadCount());
            assertEquals(0, stats.getQueryExecutionCount());
            // One additional request: lazy, select all the fields of position but no Currency this time
            // select positionen0_.* from test.position positionen0_ inner join test.currency currencyen1_ on positionen0_.gross_amount_currency_iso_code=currencyen1_.iso_code inner join test.currency currencyen2_ on positionen0_.net_amount_currency_iso_code=currencyen2_.iso_code where positionen0_.id=?
            assertEquals(3, stats.getPrepareStatementCount());
        }
    }

    @Test
    void testSelectARangeOfPositionWithEagerOrLazy() {
        // Given
        SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
        Statistics stats = sessionFactory.getStatistics();
        stats.clear();
        em.clear();

        // When first load, need to execute a query using the natural id
        List<PkPositionEntity> positions = sut.findAllByIdIn(List.of(1L, 2L, 3L, 4L));
        assertEquals(4, positions.size());
        // 4 position and 2 currencies
        assertEquals(6,
                     stats.getEntityLoadCount()); // Fail with Lazy now as it wasn't necessary to load the currency, different behavior than with naturalId
        // One Query for the search with IN clause without joining
        assertEquals(1, stats.getQueryExecutionCount());
        // Why 4 additions request 2 for 'CHF' and 2 for 'EUR' ?
        // select currencyen0_.* from test.currency currencyen0_ where currencyen0_.iso_code=?
        assertEquals(5, stats.getPrepareStatementCount());

        // Now reload
        List<PkPositionEntity> positions2 = sut.findAllByIdIn(List.of(5L, 6L));
        assertEquals(2, positions2.size());
        // 2 more rows
        assertEquals(8, stats.getEntityLoadCount());
        // One additional query for the search
        assertEquals(2, stats.getQueryExecutionCount());
        // Why this additional prepare statement ?
        assertEquals(6, stats.getPrepareStatementCount());
    }

}