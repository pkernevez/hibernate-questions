package net.kernevez.hibernate.entity.withnatural;

import net.kernevez.hibernate.conf.JpaAuditingConfiguration;
import net.kernevez.hibernate.conf.TimeZoneConfig;
import org.hibernate.SessionFactory;
import org.hibernate.stat.Statistics;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.junit.jupiter.Testcontainers;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@Import({JpaAuditingConfiguration.class, TimeZoneConfig.class})
@Transactional
@Testcontainers
class CurrencyEntityTest {
    @Autowired
    private EntityManager em;

    @Autowired
    private CurrencyRepository sut;

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Test
    void naturalIdIsWorkingWell_WhenLoadingMultipleTimeByNaturalId() {
        // Given
        SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
        Statistics stats = sessionFactory.getStatistics();
        stats.clear();
        em.clear();

        // When first load, need to execute a query using the natural id
        CurrencyEntity chf = sut.getCurrencyByNaturalIsoCode("CHF");
        assertEquals(1, chf.getId());
        assertEquals("CHF", chf.getIsoCode());
        assertEquals(1, stats.getEntityLoadCount());
        assertEquals(0, stats.getQueryExecutionCount());
        assertEquals(1, stats.getPrepareStatementCount());

        // When second load, no need to execute a query again
        CurrencyEntity chf2 = sut.getCurrencyByNaturalIsoCode("CHF");
        assertEquals(1, chf.getId());
        assertEquals("CHF", chf.getIsoCode());
        assertEquals(1, stats.getEntityLoadCount());
        assertEquals(0, stats.getQueryExecutionCount());
        assertEquals(1, stats.getPrepareStatementCount());
    }

    @Test
    void naturalIdIsWorkingWell_WhenLoadingFirstByPKThenByNaturalId() {
        // Given

        SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
        Statistics stats = sessionFactory.getStatistics();
        stats.clear();
        em.clear();

        // When first load, need to execute a query using the natural id
        CurrencyEntity chf = sut.getReferenceById(1L);
        assertEquals(1, chf.getId());
        assertEquals("CHF", chf.getIsoCode());
        assertEquals(1, stats.getEntityLoadCount());
        assertEquals(0, stats.getQueryExecutionCount());
        assertEquals(1, stats.getPrepareStatementCount());

        // When second load, no need to execute a query again
        CurrencyEntity chf2 = sut.getCurrencyByNaturalIsoCode("CHF");
        assertEquals(1, chf2.getId());
        assertEquals("CHF", chf2.getIsoCode());
        assertEquals(1, stats.getEntityLoadCount());
        assertEquals(0, stats.getQueryExecutionCount());
        assertEquals(1, stats.getPrepareStatementCount());
    }

}