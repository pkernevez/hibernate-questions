package net.kernevez.hibernate.entity.withnatural;

import net.kernevez.hibernate.conf.JpaAuditingConfiguration;
import net.kernevez.hibernate.conf.TimeZoneConfig;
import org.hibernate.SessionFactory;
import org.hibernate.stat.Statistics;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.junit.jupiter.Testcontainers;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@Import({JpaAuditingConfiguration.class, TimeZoneConfig.class})
@Transactional
@Testcontainers
class PositionEntityTest {

    @Autowired
    private EntityManager em;

    @Autowired
    private PositionRepository sut;

    @Autowired
    private EntityManagerFactory entityManagerFactory;


    @Nested
    class WithEager {
        // Those tests need to have a Eager strategy in class Amount
        //    @ManyToOne()
        //    protected CurrencyEntity currency;
        @Test
        void testPassWithAmountEagerStrategy() {
            // Given
            SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
            Statistics stats = sessionFactory.getStatistics();
            stats.clear();
            em.clear();

            // When first load, need to execute a query using the natural id
            PositionEntity pos1chf = sut.getReferenceById(1L);
            assertEquals(1, pos1chf.getId());
            assertEquals(new BigDecimal("123456.7890"), pos1chf.getNetAmount().getQuantity());
            assertEquals("CHF", pos1chf.getNetAmount().getCurrency().getIsoCode());
            assertEquals(2, stats.getEntityLoadCount());
            assertEquals(0, stats.getQueryExecutionCount());
            // First request: eager, select all the fields of position and 2 times the net and gross currencies (this row has the same currency but it's a particular case)
            // select positionen0_.*, currencyen1_.id as id1_0_1_, currencyen1_.*, currencyen2_.* from test.position positionen0_ inner join test.currency currencyen1_ on positionen0_.gross_amount_currency_iso_code=currencyen1_.iso_code inner join test.currency currencyen2_ on positionen0_.net_amount_currency_iso_code=currencyen2_.iso_code where positionen0_.id=?
            // But a second one: why Hibernate reload the CHF as it already has all the fields (twice in this case) ?
            // select currencyen0_.* from test.currency currencyen0_ where currencyen0_.iso_code=?
            assertEquals(2, stats.getPrepareStatementCount());

            // Now reload
            PositionEntity pos2chf = sut.getReferenceById(2L);
            assertEquals(2, pos2chf.getId());
            assertEquals(new BigDecimal("223456.7890"), pos2chf.getNetAmount().getQuantity());
            assertEquals("CHF", pos2chf.getNetAmount().getCurrency().getIsoCode());
            assertEquals(3, stats.getEntityLoadCount());
            assertEquals(0, stats.getQueryExecutionCount());
            // One additional request: eager, select all the fields of position and 2 times the net and gross currencies (this row has the same currency but it's a particular case)
            // select positionen0_.*, currencyen1_.id as id1_0_1_, currencyen1_.*, currencyen2_.* from test.position positionen0_ inner join test.currency currencyen1_ on positionen0_.gross_amount_currency_iso_code=currencyen1_.iso_code inner join test.currency currencyen2_ on positionen0_.net_amount_currency_iso_code=currencyen2_.iso_code where positionen0_.id=?
            assertEquals(3, stats.getPrepareStatementCount());
        }

    }

    @Nested
    class WithLazy {
        // Those tests need to have a Eager strategy in class Amount
        //    @ManyToOne(fetch = FetchType.LAZY)
        //    protected CurrencyEntity currency;

        @Test
        void testPassWithAmountLazyStrategy() {

            // Given
            SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
            Statistics stats = sessionFactory.getStatistics();
            stats.clear();
            em.clear();

            // When first load, need to execute a query using the natural id
            PositionEntity pos1chf = sut.getReferenceById(1L);
            assertEquals(1, pos1chf.getId());
            assertEquals(new BigDecimal("123456.7890"), pos1chf.getNetAmount().getQuantity());
            assertEquals("CHF", pos1chf.getNetAmount().getCurrency().getIsoCode());
            assertEquals(2, stats.getEntityLoadCount());
            assertEquals(0, stats.getQueryExecutionCount());
            // First request: lazy, select all the fields of position
            // select positionen0_.*, currencyen1_.id as id1_0_1_ from test.position positionen0_ inner join test.currency currencyen1_ on positionen0_.gross_amount_currency_iso_code=currencyen1_.iso_code inner join test.currency currencyen2_ on positionen0_.net_amount_currency_iso_code=currencyen2_.iso_code where positionen0_.id=?
            // But 2 requests with the same naturalId: why Hibernate reload the CHF twice as the same iso code (twice with 'CHF') ?
            // select currencyen0_.* from test.currency currencyen0_ where currencyen0_.iso_code=?
            assertEquals(3, stats.getPrepareStatementCount());

            // Now reload
            PositionEntity pos2chf = sut.getReferenceById(2L);
            assertEquals(2, pos2chf.getId());
            assertEquals(new BigDecimal("223456.7890"), pos2chf.getNetAmount().getQuantity());
            assertEquals("CHF", pos2chf.getNetAmount().getCurrency().getIsoCode());
            assertEquals(3, stats.getEntityLoadCount());
            assertEquals(0, stats.getQueryExecutionCount());
            // One additional request: lazy, select all the fields of position but no Currency this time
            // select positionen0_.* from test.position positionen0_ inner join test.currency currencyen1_ on positionen0_.gross_amount_currency_iso_code=currencyen1_.iso_code inner join test.currency currencyen2_ on positionen0_.net_amount_currency_iso_code=currencyen2_.iso_code where positionen0_.id=?
            assertEquals(4, stats.getPrepareStatementCount());
        }
    }

    @Test
    void testSelectARangeOfPositionWithEagerOrLazy() {
        // Given
        SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
        Statistics stats = sessionFactory.getStatistics();
        stats.clear();
        em.clear();

        // When first load, need to execute a query using the natural id
        List<PositionEntity> positions = sut.findAllByIdIn(List.of(1L, 2L, 3L, 4L));
        assertEquals(4, positions.size());
        // 4 position and 2 currencies
        assertEquals(6, stats.getEntityLoadCount());
        // One Query for the search with IN clause without joining
        assertEquals(1, stats.getQueryExecutionCount());
        // Why 4 additions request 2 for 'CHF' and 2 for 'EUR' ?
        // select currencyen0_.* from test.currency currencyen0_ where currencyen0_.iso_code=?
        assertEquals(5, stats.getPrepareStatementCount());

        // Now reload
        List<PositionEntity> positions2 = sut.findAllByIdIn(List.of(5L, 6L));
        assertEquals(2, positions2.size());
        // 2 more rows
        assertEquals(8, stats.getEntityLoadCount());
        // One additional query for the search
        assertEquals(2, stats.getQueryExecutionCount());
        // Why this additional prepare statement ?
        assertEquals(6, stats.getPrepareStatementCount());
    }

}