# The best solution is to run the tests, let's start with the link using the PK

Those tests are in the package: net.kernevez.hibernate.entity.withpk

We have a Position (PkPosition) having 2 amounts (net & gross).

Everything looks like it should.

- With Eager: Hibernate loads the position & the Currencies, without additional request
- With Lazy: Hibernate loads the position and the currencies with the minimum requests, meaning one per ID.

# Now have a look to the same with a naturalId

Those tests are in the package: net.kernevez.hibernate.entity.withnatural
It's exactly the same structure except that the relation from Amount to Currency is on the natural Id.

## Run CurrencyEntityTest: the natural Id is used without additional request

When we run the test and read the same object with the naturalId, there is only one DB request.
The L1 cache is used.

## Run PositionEntityTest#testPassWithAmountEagerStrategy

This test need to have a eager (default) strategy to pass in class Amount.

```java
@JoinColumn(referencedColumnName = "isoCode", nullable = false)
// @ManyToOne(fetch = FetchType.LAZY)
@ManyToOne()
protected CurrencyEntity currency;
```

With eager strategy every time we load a position, we inner join on currency.
The POSITION table has a large number of rows, and we only have 2 rows in CURRENCY table.

Suboptimal when loading few POSITION, but acceptable.

## Run PositionEntityTest#testPassWithAmountLazyStrategy

This test need to have a lazy strategy to pass in class Amount.

```java
@JoinColumn(referencedColumnName = "isoCode", nullable = false)
@ManyToOne(fetch = FetchType.LAZY)
// @ManyToOne()
protected CurrencyEntity currency;
```

In this case we are doing 3 requests.
The first 2 are normal, but why do we do a second loading of the 'CHF' currency ?

## Run PositionEntityTest#testSelectARangeOfPositionWithEagerOrLazy

Whatever the fetch strategy is, we always load the same currency twice.

I don't understand why we are loading currencies with sub requests like in `PositionEntityTest#testPassWithAmountEagerStrategy`
(all fields are already loaded with eager strategy).

And why are the 2 currencies loaded twice ?
Twice CHF and twice EUR for 4 rows.
Why is it not 2 (one per currency) or 8 twice per row ?

## Run PositionEntityTest#testSelectARangeOfPositionWithLazy

With a range it's quite different.
Even with Lazy the Currency is always requested even we are not looking at the currency fields.

# If you want to run the application

## Start a Postgres

```bash
# x86
docker run -it -p 5432:5432 -e POSTGRES_PASSWORD=test_user -e POSTGRES_USER=test_user -e POSTGRES_DB=db \                                           (base)
  -v $(PWD)/src/test/resources/schema.sql:/docker-entrypoint-initdb.d/create_postgresql_schema.sql  bitnami/postgresql:13.5.0

# Mac ARM
docker run --platform linux/amd64 -it -p 5432:5432 -e POSTGRES_PASSWORD=test_user -e POSTGRES_USER=test_user -e POSTGRES_DB=db \                                           (base)
  -v $(PWD)/src/test/resources/schema.sql:/docker-entrypoint-initdb.d/create_postgresql_schema.sql  bitnami/postgresql:13.5.0
```

## Start the application with postgres profile

```bash
mvn spring-boot:run -Dspring-boot.run.profiles=postgres
```

